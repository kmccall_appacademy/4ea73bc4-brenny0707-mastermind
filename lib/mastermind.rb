class Code
  PEGS = {"r" => "red", "g" => "green", "b" => "blue", "y" => "yellow", "o" => "orange", "p" => "purple"}

  def initialize(pegs)
    @pegs = pegs
  end


  attr_reader :pegs

  def self.parse(string)
    @parse = string
    pegs = @parse.downcase.split("").map do |ch|
      raise 'error' if PEGS[ch].nil?
      PEGS[ch]
    end
    Code.new(pegs)
  end

  def self.random
    random_pegs = []
    4.times do
      random_pegs << PEGS.values.sample
    end
    Code.new(random_pegs)
  end

  def [](i)
    @pegs[i]
  end

  def exact_matches(code2)
    exact_count = 0
    self.pegs.each_with_index do |color, idx|
      exact_count += 1 if color == code2[idx]
    end
    exact_count
  end

  def near_matches(code2)
    near_count = 0
    near_colors = []
    self.pegs.each_with_index do |color, idx|
      if code2.pegs.include?(color) && color != code2[idx] && !near_colors.include?(color)
        near_count += 1
        near_colors << color
      end
    end
    near_count - exact_matches(code2)
  end

  def ==(code2)
    return true if exact_matches(code2) == 4
    false
  end
end #Code

class Game

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end
  attr_reader :secret_code

  def get_guess
    puts "Please pick four colors: (r)ed, (g)reen, (b)lue, (y)ellow, (o)range, (p)urple."
    guess_code = Code.parse($stdin.gets.chomp)
  end

  def display_matches(guess)
    puts "exact_matches: #{@secret_code.exact_matches(guess)}"
    puts "near_matches: #{@secret_code.near_matches(guess)}"
  end

end
